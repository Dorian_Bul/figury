package figpak;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// bufor
	Image image;
	// wykreslacz ekranowy
	Graphics2D device;
	// wykreslacz bufora
	Graphics2D buffer;

	ArrayList<Thread> lista = new ArrayList<Thread>();
	
	public static int delay = 30;

	public static Timer timer;
	AnimatorApp ramka;
	
	private static int numer = 0;

	public AnimPanel() {
		super();
		setBackground(Color.WHITE);
		timer = new Timer(delay, this);
	}

	public AnimPanel(AnimatorApp animatorApp) {
		super();
		setBackground(Color.WHITE);
		timer = new Timer(delay, this);
		ramka = animatorApp;
	}

	@SuppressWarnings("deprecation")
	public  void Nowyrozmiar(AnimatorApp animatorApp)
	{
		ramka = animatorApp;
		
		for( Thread i: lista)
		{
			i.stop();
		}
		
		lista.removeAll(lista);
		
		setBounds(10, 11, ramka.getWidth()-35,  ramka.getHeight()-100);
		image = createImage( ramka.getWidth()-35, ramka.getHeight()-100);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		buffer.setBackground(Color.WHITE);
		
	}
	
	public void initialize(int x, int y) {
		int width = x;
		int height = y;

		image = createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		buffer.setBackground(Color.WHITE);
		

	}

	void addFig() {
		Figura fig = null;
		numer++;
		if(numer % 3 == 0) fig = new Kwadrat(buffer, delay, ramka.getWidth()-35, ramka.getHeight()-100);
		else if(numer % 3 == 1) fig = new Elipsa(buffer, delay, ramka.getWidth()-35, ramka.getHeight()-100);
		else if(numer % 3 == 2) fig = new Trojkat(buffer, delay, ramka.getWidth()-35, ramka.getHeight()-100);
		//Figura fig = (numer++ % 2 == 0) ? new Kwadrat(buffer, delay, ramka.getWidth()-35, ramka.getHeight()-100)
		//		: new Elipsa(buffer, delay, ramka.getWidth()-35, ramka.getHeight()-100);
		timer.addActionListener(fig);
		Thread watek = new Thread(fig);
		lista.add(watek);
		watek.start();
	}

	void animate() {
		if (timer.isRunning()) {
			timer.stop();
		} else {
			timer.start();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		device.drawImage(image, 0, 0, null);
		if(Figura.gradient==0) buffer.clearRect(0, 0, ramka.getWidth()-35, ramka.getHeight()-100);
	}
}
