/**
 * 
 */
package figpak;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.util.Random;

/**
 * @author tb
 *
 */
public abstract class Figura implements Runnable, ActionListener {

	// wspolny bufor
	protected Graphics2D buffer;
	protected Area area;
	// do wykreslania
	protected Shape shape;
	// przeksztalcenie obiektu
	protected AffineTransform aft;

	// przesuniecie
	private int dx, dy;
	// rozciaganie
	private double sf;
	// kat obrotu
	private double an;
	private int delay;
	private int width;
	private int height;
	private Color clr;
	static public int gradient=0;

	protected static final Random rand = new Random();

	public Figura(Graphics2D buf, int del, int w, int h) {
		delay = del;
		buffer = buf;
		width = w;
		height = h;

		dx = 1 + rand.nextInt(5);
		dy = 1 + rand.nextInt(5);
		sf = 1 + 0.05 * rand.nextDouble();
		an = 0.1 * rand.nextDouble();

		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
		// reszta musi być zawarta w realizacji klasy Figure
		// (tworzenie figury i przygotowanie transformacji)
	}

	@Override
	public void run() {
		// przesuniecie na srodek
		aft.translate(50, 50);
		area.transform(aft);
		shape = area;

		while (true) {
			// przygotowanie nastepnego kadru
			if(AnimPanel.timer.isRunning()) shape = nextFrame();
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
			}
		}
	}

	static public void zmiana(){
		if(gradient==0) {
			gradient=1;
			AnimPanel.delay = 2;
		}
		else {
			gradient =0;
			AnimPanel.delay = 30;
		}
		
	}

	protected Shape nextFrame() {
		// zapamietanie na zmiennej tymczasowej
		// aby nie przeszkadzalo w wykreslaniu
		area = new Area(area);
		aft = new AffineTransform();
		Rectangle bounds = area.getBounds();
		int cx = bounds.x + bounds.width / 2;
		int cy = bounds.y + bounds.height / 2;
		// odbicie
		if (cx < 0+bounds.width / 2 || cx > width-bounds.width / 2)
			dx = -dx;
		if (cy < 0+ bounds.height / 2 || cy > height- bounds.height / 2)
			dy = -dy;
		// zwiekszenie lub zmniejszenie
		if (bounds.height > height / 3 || bounds.height < 10)
		{
			sf = 1 / sf;
		}
		// konstrukcja przeksztalcenia
		aft.translate(cx, cy);
		aft.scale(sf, sf);
		aft.rotate(an);
		aft.translate(-cx, -cy);
		aft.translate(dx, dy);
		// przeksztalcenie obiektu
		area.transform(aft);
		return area;
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		// wypelnienie obiektu
		if(gradient==0) buffer.setColor(clr.brighter());
		else buffer.setPaint(new GradientPaint(0, 0, new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)), 20, 20, new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)), true));
		buffer.fill(shape);
		// wykreslenie ramki
		if(gradient==0) buffer.setColor(clr.darker());
		else buffer.setPaint(new GradientPaint(0, 0, new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)), 20, 20, new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)), true));
		buffer.draw(shape);
	}

}
