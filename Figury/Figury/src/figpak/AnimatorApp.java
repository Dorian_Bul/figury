package figpak;

import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.JButton;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ActionEvent;
import javax.swing.BorderFactory;
import java.awt.Color;

public class AnimatorApp extends JFrame implements ComponentListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	AnimPanel kanwa;
	JButton btnAdd ;
	JButton btnAnimate;
	JButton btnAddXX;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final AnimatorApp frame = new AnimatorApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param delay 
	 */
	public AnimatorApp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int ww = 450, wh = 300;
		setBounds((screen.width-ww)/2, (screen.height-wh)/2, ww, wh);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		addComponentListener(this);
		
		kanwa = new AnimPanel(this);
		kanwa.setBounds(10, 11, getWidth()-35, getHeight()-100);
		kanwa.setBorder(BorderFactory.createLineBorder(Color.black));
		contentPane.add(kanwa);
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				kanwa.initialize(getWidth()-35, getHeight()-100);
			}
		});
		
		btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.addFig();
			}
		});
		btnAdd.setBounds(10, getHeight()-75, 80, 23);
		contentPane.add(btnAdd);
		
		btnAnimate = new JButton("Animate");
		btnAnimate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.animate();
			}
		});
		btnAnimate.setBounds(100, getHeight()-75, 80, 23);
		contentPane.add(btnAnimate);

		btnAddXX = new JButton("Tryb LSD");
		btnAddXX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				Figura.zmiana();
			}
		});
		btnAddXX.setBounds(190, getHeight()-75, 120, 23);
		contentPane.add(btnAddXX);
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentResized(ComponentEvent arg0) {
		
		kanwa.Nowyrozmiar(this);
		btnAdd.setBounds(10, getHeight()-75, 80, 23);
		btnAnimate.setBounds(100, getHeight()-75, 80, 23);
		btnAddXX.setBounds(190, getHeight()-75, 120, 23);
		
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
